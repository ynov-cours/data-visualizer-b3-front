import React, { useState, useEffect } from 'react';
import GoogleMap from 'google-map-react';
import MapMarker from './MapMarker';
import '../sass/main.scss';

function RowModal(props){

  const [firstname, setFirstname] = useState("");

  return (
    <div className="modal fade" id={props.id} tabindex="-1" role="dialog" aria-labelledby="person-modal-label" aria-hidden="true">
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">Modifier les informations de {props.title}</h5>
            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body overlay-auto">
            <form onSubmit={props.modalSubmitCallback}>
              <div class="form-group">
                <label className="mt-3 mb-0" for="setNewFirstName">Prénom</label>
                <input type="text" class="form-control" id="setNewFirstName" onChange={(e) => {console.log(e.target.value);props.modalCallback(`${e.target.value}`)}} placeholder={props.person != undefined ? props.person.firstname : ""}/>
                <label className="mt-3 mb-0" for="setNewLastName">Nom</label>
                <input type="text" class="form-control" id="setNewLastName" placeholder={props.person != undefined ? props.person.lastname : ""}/>
                <label className="mt-3 mb-0" for="setNewEmail">Email</label>
                <input type="email" class="form-control" id="setNewEmail" placeholder={props.person != undefined ? props.person.contact.email : ""}/>
                <label className="mt-3 mb-0" for="setNewPhone">Téléphone</label>
                <input type="text" class="form-control" id="setNewPhone" placeholder={props.person != undefined ? props.person.contact.phone : ""}/>
                <label className="mt-3 mb-0" for="setNewTown">Ville</label>
                <input type="text" class="form-control" id="setNewTown" placeholder={props.person != undefined ? props.person.contact.city : ""}/>
                <label className="mt-3 mb-0" for="setNewCountry">Pays</label>
                <input type="text" class="form-control" id="setNewCountry" placeholder={props.person != undefined ? props.person.contact.country : ""}/>
                <label className="mt-3 mb-0" for="setNewPet">Animal</label>
                <input type="text" class="form-control" id="setNewPet" placeholder={props.person != undefined ? props.person.preferences.favorite_pet : ""}/>
                <label className="mt-3 mb-0" for="setNewFruit">Fruit</label>
                <input type="text" class="form-control" id="setNewFruit" placeholder={props.person != undefined ? props.person.preferences.favorite_fruit : ""}/>
                <label className="mt-3 mb-0" for="setNewMovie">Film</label>
                <input type="text" class="form-control" id="setNewMovie" placeholder={props.person != undefined ? props.person.preferences.favorite_movie : ""}/>
                <label className="mt-3 mb-0" for="setNewColor">Couleur</label>
                <input type="text" class="form-control" id="setNewColor" placeholder={props.person != undefined ? props.person.preferences.favorite_color : ""}/>
              </div>
              <button type="submit" className="btn btn-primary">Sauvegarder</button>
            </form>

            <div style={{height:"200px"}} className="mt-3">
              <GoogleMap
                // apiKey={AIzaSyA-mQAezaw5pJyciCy6di7T2cN0QNH2_bE} // set if you need stats etc ...
                center={[props.lat, props.lon]}
                zoom={3}>
                <MapMarker lat={props.lat} lng={props.lon}></MapMarker>

              </GoogleMap>
            </div>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" data-dismiss="modal">Fermer</button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default RowModal;

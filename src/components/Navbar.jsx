import React, { Component } from 'react';
import * as BIcons from 'react-icons/bs';
import * as FIcons from 'react-icons/fa';
import '../sass/navbar.scss';

class Navbar extends Component{
  render(){
    return(
      <div id="navbar" className="col-lg-2 navigation p-0 p-3 d-flex flex-column">
        <div className="nav-header">
          <p className="title">Data Visualizer</p>
          <p className="text-truncate">B3 Info - Avagliano Enzo</p>
        </div>
        <div className="nav-content">
          <div className="nav-body d-flex flex-column justify-content-center">
            <a id="nav-link-home" href="/" className="nav-link p-1 my-2 active"><BIcons.BsChevronDoubleRight/> Home</a>
            <a id="nav-link-graphs" href="/graphs" className="nav-link p-1 my-2"><BIcons.BsChevronDoubleRight/> Graphs</a>
            <a id="nav-link-search" href="/search" className="nav-link p-1 my-2"><BIcons.BsChevronDoubleRight/> Search & Modify</a>
            <a id="nav-link-filter" href="/filter" className="nav-link p-1 my-2"><BIcons.BsChevronDoubleRight/> Filter</a>
            <a id="nav-link-map" href="/map" className="nav-link p-1 my-2"><BIcons.BsChevronDoubleRight/> Map</a>
          </div>
          <div className="nav-footer d-flex flex-column justify-content-end align-items-center">
            <div className="d-flex flex-row">
              <a className="nav-link themed" href="https://gitlab.com/EloxFire"><FIcons.FaGitlab/></a>
              <a className="nav-link themed" href="https://twitter.com/EloxFire"><FIcons.FaTwitter/></a>
            </div>
            <div>
              &copy; Enzo Avagliano - {new Date().getFullYear()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Navbar;

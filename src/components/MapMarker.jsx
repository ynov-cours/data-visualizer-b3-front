import React from 'react';


function MapMarker(props){
  return(
    <div style={{width:"15px", height:"15px", backgroundColor:"#9146FF", borderRadius:"50%", border: "1px solid #000"}}>
      {props.text}
    </div>
  )
}

export default MapMarker;

import { render, screen } from '@testing-library/react';
import App from './App';

test('Renders App', () => {
  render(<App />);
  const app = document.getElementById('app');
  expect(app).toBeInTheDocument();
});

test('Renders navbar', () => {
  render(<App />);
  const navbar = document.getElementById('navbar');
  expect(navbar).toBeInTheDocument();
});

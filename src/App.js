/**
 * @Author: Enzo Avagliano <EloxFire>
 * @Date:   2021-01-27T20:06:30+01:00
 * @Email:  enzo.avagliano@ynov.com
 * @Project: TP Data visualizer - B3
 * @Last modified by:   EloxFire
 * @Last modified time: 2021-03-09T12:09:05+01:00
 * @Copyright: Enzo Avagliano
 */

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './sass/main.scss';
import Dashboard from './pages/Dashboard';
import Graph from './pages/Graph';
import Search from './pages/Search';
import Filter from './pages/Filter';
import Map from './pages/Map';

function App() {
  return (
    <div id="app">
      <Router>
        <Switch>
          <Route path="/" exact component={props =>
            <div>
              <Dashboard/>
            </div>
          }/>

          <Route path="/overview" exact component={props =>
            <div className="content d-flex flex-row overflow-auto">
              <Dashboard/>
            </div>
          }/>

          <Route path="/graphs" exact component={props =>
            <div className="content d-flex flex-row overflow-auto">
              <Graph/>
            </div>
          }/>

          <Route path="/search" exact component={props =>
            <div className="content d-flex flex-row overflow-auto">
              <Search/>
            </div>
          }/>

          <Route path="/filter" exact component={props =>
            <div className="content d-flex flex-row overflow-auto">
              <Filter/>
            </div>
          }/>

          <Route path="/map" exact component={props =>
            <div className="content d-flex flex-row overflow-auto">
              <Map/>
            </div>
          }/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;

/**
 * @Author: Enzo Avagliano <EloxFire>
 * @Date:   2021-02-16T18:33:56+01:00
 * @Email:  enzo.avagliano@ynov.com
 * @Project: TP Data visualizer - B3
 * @Last modified by:   EloxFire
 * @Last modified time: 2021-03-09T14:43:47+01:00
 * @Copyright: Enzo Avagliano
 */

import React, { useState, useEffect } from 'react';
import Navbar from '../components/Navbar';
import * as FIcons from 'react-icons/fa';
import '../sass/main.scss';

const API_CONF = require('../api-conf.js');

function Dashboard() {

  const [peoples, setPeoples] = useState([]);
  const [subArray, setSubArray] = useState([]);

  let startOffset = 1;
  let endOffset = 10;

  useEffect(() => {
    fetch(`${API_CONF.API_URL}`)
    .then((response) => response.json())
    .then((response) => {
      console.log(response);
      setPeoples(response.people)
      setSubArray(response.people.slice(startOffset - 1, endOffset));
    });

    document.getElementById("nav-link-home").classList.add('active');
    document.getElementById("nav-link-graphs").classList.remove('active');
    document.getElementById("nav-link-filter").classList.remove('active');
    document.getElementById("nav-link-search").classList.remove('active');
    document.getElementById('nav-link-map').classList.remove('active');
  }, []);

  const handleRowClick = (index) => {
    let row = document.querySelector(`.data-row-${index}`);
    let classes = row.getAttribute('class');
    let res = Array.from(document.querySelector(`.data-row-${index}`).children, ({textContent}) => textContent.trim()).filter(Boolean).join(', ');
    console.log(res);
  }


  return(
    <div id="dashboard" className="d-flex flex-column flex-lg-row col-12">
      <Navbar/>
      <div className="col-lg-10 p-0 p-3 d-flex flex-column">
        <div className="dash-tab top-infos p-3 mb-3">
          <p></p>
          <p><FIcons.FaExternalLinkAlt/> API URL : <a href={API_CONF.API_URL} target="_blank" rel="noreferrer">{API_CONF.API_URL}</a></p>
        </div>
        <div className="dash-tab main-tab p-3 mb-3 table-responsive">
          <table id="all-table" className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Sexe</th>
                <th scope="col">Email</th>
                <th scope="col">Adresse</th>
                <th scope="col">Ville</th>
                <th scope="col">Pays</th>
                <th scope="col">Téléphone</th>
                <th scope="col">Position</th>
                <th scope="col">Animal préféré</th>
                <th scope="col">Fruit préféré</th>
                <th scope="col">Couleur préférée</th>
                <th scope="col">Film préféré</th>
              </tr>
            </thead>
            <tbody>
              {
                subArray.map(function(item, index){
                  return(
                    <tr className={`data-row data-row-${item.id}`} onClick={() => handleRowClick(item.id)} key={index}>
                      <th scope="row">{item.id}</th>
                      <td className={`data-content-lastname-${item.id}`}>{item.lastname}</td>
                      <td className={`data-content-firstname-${item.id}`}>{item.firstname}</td>
                      <td className={`data-content-gender-${item.id}`}>{item.gender}</td>
                      <td className={`data-content-email-${item.id}`}>{item.contact.email}</td>
                      <td className={`data-content-address-${item.id}`}>{item.contact.address}</td>
                      <td className={`data-content-city-${item.id}`}>{item.contact.city}</td>
                      <td className={`data-content-country-${item.id}`}>{item.contact.country}</td>
                      <td className={`data-content-phone-${item.id}`}>{item.contact.phone}</td>
                      <th className={`data-content-location-${item.id}`}>{item.contact.location.lon} {item.contact.location.lat}</th>
                      <td className={`data-content-pet-${item.id}`}>{item.preferences.favorite_pet}</td>
                      <td className={`data-content-fruit-${item.id}`}>{item.preferences.favorite_fruit}</td>
                      <td className={`data-content-color-${item.id}`}>{item.preferences.favorite_color}</td>
                      <td className={`data-content-movie-${item.id}`}>{item.preferences.favorite_movie}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
          {/* <Pagination id="1"/> */}
          <div>
            <nav aria-label="Global table pagination">
              <ul className="pagination">
                {/* <button className="page-item rounded-left page-link inverted-themed">
                <span aria-hidden="true">&laquo;</span>
                <span className="sr-only">Previous</span>
              </button> */}
              <button onClick={() => {setSubArray(peoples.slice(0, 10))}} className="page-item page-link inverted-themed rounded-left">1</button>
              <button onClick={() => setSubArray(peoples.slice(10, 20))} className="page-item page-link inverted-themed">2</button>
              <button onClick={() => setSubArray(peoples.slice(20, 30))} className="page-item page-link inverted-themed">3</button>
              <button onClick={() => setSubArray(peoples.slice(30, 40))} className="page-item page-link inverted-themed">4</button>
              <button onClick={() => setSubArray(peoples.slice(40, 50))} className="page-item page-link inverted-themed">5</button>
              <button onClick={() => setSubArray(peoples.slice(50, 60))} className="page-item page-link inverted-themed">6</button>
              <button onClick={() => setSubArray(peoples.slice(60, 70))} className="page-item page-link inverted-themed">7</button>
              <button onClick={() => setSubArray(peoples.slice(80, 90))} className="page-item page-link inverted-themed">8</button>
              <button onClick={() => setSubArray(peoples.slice(90, 100))} className="page-item page-link inverted-themed rounded-right">9</button>
              {/* <button className="page-item rounded-right page-link inverted-themed">
              <span aria-hidden="true">&raquo;</span>
              <span className="sr-only">Next</span>
            </button> */}
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>
)
}

export default Dashboard;

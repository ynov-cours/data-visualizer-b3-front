/**
* @Author: Enzo Avagliano <EloxFire>
* @Date:   2021-03-09T12:07:39+01:00
* @Email:  enzo.avagliano@ynov.com
* @Project: TP Data visualizer - B3
 * @Last modified by:   EloxFire
 * @Last modified time: 2021-03-09T15:43:29+01:00
* @Copyright: Enzo Avagliano
*/

import React, { useState, useEffect } from 'react';
import Navbar from '../components/Navbar';
import RowModal from '../components/RowModal';
import $ from 'jquery';
import '../sass/main.scss';

const API_CONF = require('../api-conf.js');

function Search() {

  const [peoples, setPeoples] = useState([]);
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedRowId, setSelectedRowId] = useState(null);

  // Modal attributes list
  const [modalAttributes, setModalAttributes] = useState(
    {
      id:"person-modal",
      template: peoples[1]
    }
  );
  // Infos from modal chil components
  const [newFirstname, setNewFirstname] = useState("");

  useEffect(() => {
    fetch(`${API_CONF.API_URL}`)
    .then((response) => response.json())
    .then((response) => {
      setPeoples(response.people)
    });

    document.getElementById("nav-link-home").classList.remove('active');
    document.getElementById("nav-link-graphs").classList.remove('active');
    document.getElementById("nav-link-filter").classList.remove('active');
    document.getElementById("nav-link-search").classList.add('active');
    document.getElementById('nav-link-map').classList.remove('active');
  }, []);

  //JSON to download
  let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(peoples));

  const handleRowClick = (index) => {
    setSelectedRowId(index-1);
    let res = Array.from(document.querySelector(`.data-row-${index}`).children, ({textContent}) => textContent.trim()).filter(Boolean).join(', ');
    console.log(res);

    let modal = $('#person-modal');
    // On passe a notre fenetre popup les attributs de la ligne que nous venons de select dans notre tableau
    setModalAttributes({
      id: "person-modal",
      person: peoples[index-1],
      modalCallback: setNewInfos,
      modalSubmitCallback: submitNewInfos,
      lat: peoples[index-1].contact.location.lat,
      lon: peoples[index-1].contact.location.lon,
    });
    console.log(modalAttributes);
    modal.modal('show');

  }

  const setNewInfos = (modalData) => {
    setNewFirstname(modalData);
  }

  const submitNewInfos = (e) => {
    e.preventDefault();
    console.log(peoples);
    console.log(selectedRowId);
    console.log("New first name", newFirstname);
    // peoples[selectedRowId].firstname = newFirstname;
  }


  return(
    <div id="search" className="d-flex flex-column flex-lg-row col-12">
      <Navbar/>
      <div className="col-lg-10 p-0 p-3 d-flex flex-column">
        <div className="dash-tab top-infos p-3 mb-3">
          <div className="form-group row m-0">
            <div className="col-sm-10">
              <input onChange={(e) => {setSearchQuery(e.target.value);console.log("SearchQuery :",searchQuery)}} type="text" className="form-control" id="inputSearch" placeholder="Recherchez n'importe quoi ici"/>
            </div>
            <div className="col-sm-2">
              <a id="downloadJSON" download="TP-Data-Download.json" type="button" href={dataStr} className="btn btn-info m-0">Export data <small>(JSON)</small></a>
            </div>
          </div>
        </div>
        <div className="dash-tab main-tab p-3 mb-3 table-responsive">
          <table id="all-table" className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Sexe</th>
                <th scope="col">Email</th>
                <th scope="col">Adresse</th>
                <th scope="col">Ville</th>
                <th scope="col">Pays</th>
                <th scope="col">Téléphone</th>
                <th scope="col">Position</th>
                <th scope="col">Animal préféré</th>
                <th scope="col">Fruit préféré</th>
                <th scope="col">Couleur préférée</th>
                <th scope="col">Film préféré</th>
              </tr>
            </thead>
            <tbody>
              {
                // Fonction de filtrage sur tous les champs
                peoples.filter(item => {
                  if(searchQuery !== "" && item.lastname.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.firstname.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.gender.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.contact.email.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.contact.address.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.contact.city.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.contact.country.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.contact.phone.toString().toLowerCase().includes(searchQuery.toLowerCase())
                  || item.contact.location.lon.toString().toLowerCase().includes(searchQuery.toLowerCase())
                  || item.contact.location.lat.toString().toLowerCase().includes(searchQuery.toLowerCase())
                  || item.preferences.favorite_pet.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.preferences.favorite_fruit.toLowerCase().includes(searchQuery.toLowerCase())
                  || item.preferences.favorite_movie.toLowerCase().includes(searchQuery.toLowerCase())){
                    // console.log("Filter response : true");
                    // console.log("Filter item",item);
                    return item;
                  }
                }).map((item, index) => (
                    <tr key={index} className={`data-row data-row-${item.id} data-item data-item-${item.id}`} onClick={() => handleRowClick(item.id)}>
                      <th scope="row">{item.id}</th>
                      <td className={`data-content-lastname-${item.id}`}>{item.lastname}</td>
                      <td className={`data-content-firstname-${item.id}`}>{item.firstname}</td>
                      <td className={`data-content-gender-${item.id}`}>{item.gender}</td>
                      <td className={`data-content-email-${item.id}`}>{item.contact.email}</td>
                      <td className={`data-content-address-${item.id}`}>{item.contact.address}</td>
                      <td className={`data-content-city-${item.id}`}>{item.contact.city}</td>
                      <td className={`data-content-country-${item.id}`}>{item.contact.country}</td>
                      <td className={`data-content-phone-${item.id}`}>{item.contact.phone}</td>
                      <th className={`data-content-location-${item.id}`}>{item.contact.location.lon} {item.contact.location.lat}</th>
                      <td className={`data-content-pet-${item.id}`}>{item.preferences.favorite_pet}</td>
                      <td className={`data-content-fruit-${item.id}`}>{item.preferences.favorite_fruit}</td>
                      <td className={`data-content-color-${item.id}`}>{item.preferences.favorite_color}</td>
                      <td className={`data-content-movie-${item.id}`}>{item.preferences.favorite_movie}</td>
                    </tr>
                  )
                )
              }
            </tbody>
          </table>
        </div>

        <RowModal {...modalAttributes}/>
      </div>
    </div>
  )
}

export default Search;

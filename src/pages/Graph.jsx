import React, { useState, useEffect } from 'react';
import Navbar from '../components/Navbar';
import { PieChart } from 'react-minimal-pie-chart';
import { Bar, defaults } from 'react-chartjs-2';
import '../sass/main.scss';

const API_CONF = require('../api-conf.js');

function Graph() {

  const [peoples, setPeoples] = useState([]);

  useEffect(() => {
    fetch(`${API_CONF.API_URL}`)
    .then((response) => response.json())
    .then((response) => {
      setPeoples(response.people);
    });

    document.getElementById("nav-link-home").classList.remove('active');
    document.getElementById("nav-link-graphs").classList.add('active');

    // CHANGE CHARTS FONT COLOE TO WHITE
    defaults.global.defaultFontColor = "#FFFFFF";

  }, []);

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  // let distinctPets = [];
  // let petsDataset = {};
  // const getPetsList = () => {
  //   const distinct = (value, index, self) => {
  //     return self.indexOf(value) === index;
  //   }
  //
  //   distinctPets = peoples.map(x => x.preferences.favorite_pet).filter(distinct);
  //   for (let i = 0; i < distinctPets.length; i++) {
  //     petsDataset = {
  //       labels: [distinctPets],
  //       datasets: [
  //         {
  //           label: distinctPets[i],
  //           backgroundColor: `rgb(${getRandomInt(1, 255)},${getRandomInt(1, 255)},${getRandomInt(1, 255)})`,
  //           data: [peoples.filter(value => value.preferences.favorite_pet === distinctPets[i]).length]
  //         }
  //       ]
  //     }
  //   }
  //   console.log(petsDataset);
  //   console.log(distinctPets);
  // }


  // FOR PIE CHART
  const defaultLabelStyle = {
    fontSize: '7px',
    fontFamily: 'sans-serif',
  };


  return(
    <div id="graph" className="d-flex flex-column flex-lg-row col-12">
      <Navbar/>
      <div className="scrolling col-12 col-lg-10 p-0 p-3 d-flex flex-column flex-lg-row justify-content-around">
        {/* <button className="btn btn-success" onClick={(e) => {getPetsList()}}>Refresh</button> */}
        <div className="graph-pane dash-tab p-3 d-flex flex-column col-5">
          <h3>Gender repartition</h3>
          <div>
            <PieChart
              data={[
                { title: 'Female', value: peoples.filter(value => value.gender === "Female").length, color: '#badc58' },
                { title: 'Male', value: peoples.filter(value => value.gender === "Female").length, color: '#1abc9c' }
              ]}
              radius={50}
              label={({ dataEntry }) => dataEntry.title + " : " + Math.round(dataEntry.percentage) + '%'}
              labelStyle={defaultLabelStyle}
              style={{height: '30vh'}}
            />
          </div>
        </div>
        <div className="graph-pane dash-tab p-3 d-flex flex-column col-5">
          <h3>Favourite animals chart :</h3>
          <Bar
            data={
              {labels: ['Prefered animals'],
              datasets: [
                {
                  label: 'Cat',
                  backgroundColor: '#d35400',
                  data: [peoples.filter(value => value.preferences.favorite_pet === "Cat").length]
                },
                {
                  label: 'Dog',
                  backgroundColor: '#ffeaa7',
                  data: [peoples.filter(value => value.preferences.favorite_pet === "Dog").length]
                },
                {
                  label: 'Rat',
                  backgroundColor: '#6ab04c',
                  data: [peoples.filter(value => value.preferences.favorite_pet === "Rat").length]
                },
                {
                  label: 'Bird',
                  backgroundColor: '#ff7979',
                  data: [peoples.filter(value => value.preferences.favorite_pet === "Bird").length]
                },
              ]}
            }
            width={700}
            height={350}
            options={
              {
                maintainAspectRatio: true,
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true
                      },
                      scaleLabel: {
                        display: true,
                        labelString: "People"
                      }
                    }
                  ]
                }
              }
            }
            backgroundColor={"#rgba(255, 255, 255, 1)"}
            defaultColor="#FFFFFF"
          />
          {/* <Bar
            data={petsDataset}
            width={700}
            height={350}
            options={
              {
                maintainAspectRatio: true,
                scales: {
                  yAxes: [
                    {
                      ticks: {
                        beginAtZero: true
                      },
                      scaleLabel: {
                        display: true,
                        labelString: "People"
                      }
                    }
                  ]
                }
              }
            }
            backgroundColor={"#rgba(255, 255, 255, 1)"}
            defaultColor="#FFFFFF"
          /> */}
        </div>
      </div>
    </div>
  )
}

export default Graph;

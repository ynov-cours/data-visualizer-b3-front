/**
* @Author: Enzo Avagliano <EloxFire>
* @Date:   2021-02-16T18:33:56+01:00
* @Email:  enzo.avagliano@ynov.com
* @Project: TP Data visualizer - B3
* @Last modified by:   EloxFire
* @Last modified time: 2021-03-09T14:43:47+01:00
* @Copyright: Enzo Avagliano
*/

import React, { useState, useEffect } from 'react';
import Navbar from '../components/Navbar';
import GoogleMap from 'google-map-react';
import MapMarker from '../components/MapMarker';
import * as FIcons from 'react-icons/fa';
import '../sass/main.scss';

const API_CONF = require('../api-conf.js');

function Map(props) {

  const [peoples, setPeoples] = useState([]);

  useEffect(() => {
    fetch(`${API_CONF.API_URL}`)
    .then((response) => response.json())
    .then((response) => {
      setPeoples(response.people)
    });

    document.getElementById("nav-link-home").classList.remove('active');
    document.getElementById("nav-link-graphs").classList.remove('active');
    document.getElementById("nav-link-filter").classList.remove('active');
    document.getElementById("nav-link-search").classList.remove('active');
    document.getElementById('nav-link-map').classList.add('active');
  }, []);


  return(
    <div id="map" className="d-flex flex-column flex-lg-row col-12">
      <Navbar/>
      <div className="col-lg-10 p-0 p-3 d-flex flex-column">
        <div className="dash-tab top-infos p-3 mb-3">
          <p></p>
          <p><FIcons.FaExternalLinkAlt/> API URL : <a href={API_CONF.API_URL} target="_blank" rel="noreferrer">{API_CONF.API_URL}</a></p>
        </div>
        <div className="dash-tab p-3">
          <div className="map-container">
            <GoogleMap
              // apiKey={AIzaSyA-mQAezaw5pJyciCy6di7T2cN0QNH2_bE} // set if you need stats etc ...
              center={[43.2965, 5.3698]}
              zoom={1}>
              {
                peoples.map((item, index) => {
                  return(
                    <MapMarker key={index} lat={item.contact.location.lat} lng={item.contact.location.lon}></MapMarker>
                  )
                })
              }

            </GoogleMap>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Map;

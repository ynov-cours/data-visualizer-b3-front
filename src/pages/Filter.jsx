import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Navbar from '../components/Navbar';
import * as FIcons from 'react-icons/fa';
import '../sass/main.scss';

const API_CONF = require('../api-conf.js');

function Filter() {

  const [peoples, setPeoples] = useState([]);
  // const [subArray, setSubArray] = useState([]);

  // Filters
  const [uniqueFilms, setUniqueFilms] = useState([]);
  const [uniquePets, setUniquePets] = useState([]);
  const [uniqueColors, setUniqueColors] = useState([]);
  const [uniqueFruits, setUniqueFruits] = useState([]);
  const [uniqueTown, setUniqueTown] = useState([]);
  const [uniqueCountry, setUniqueCountry] = useState([]);

  // Form filters
  const [filters, setSelectedFilters] = useState([]);

  useEffect(() => {
    fetch(`${API_CONF.API_URL}`)
    .then((response) => response.json())
    .then((response) => {
      // console.log(response);
      setPeoples(response.people);
    });

    // getUniqueFilms();
    getUniquePets();
    // getUniqueColors();
    // getUniqueFruit();

    document.getElementById("nav-link-home").classList.remove('active');
    document.getElementById("nav-link-graphs").classList.remove('active');
    document.getElementById("nav-link-filter").classList.add('active');
    document.getElementById("nav-link-search").classList.remove('active');
    document.getElementById('nav-link-map').classList.remove('active');
  }, [uniqueColors]);

  const handleRowClick = (index) => {
    let row = document.querySelector(`.data-row-${index}`);
    let classes = row.getAttribute('class');
    let res = Array.from(document.querySelector(`.data-row-${index}`).children, ({textContent}) => textContent.trim()).filter(Boolean).join(', ');
    console.log(res);
  }

  // GET UNIQUES VALUES (MERCI ALEXANDRE BG !!!!!)
  let distinctPets = [];
  const getUniquePets = () => {
    const distinct = (value, index, self) => {
      return self.indexOf(value) === index;
    }

    distinctPets = peoples.map(pet => pet.preferences.favorite_pet).filter(distinct);
    setUniquePets(distinctPets);
    console.log(distinctPets);
    console.log(uniquePets);
  }

  const handleChangeFilters = (e) => {
    e.preventDefault();
    console.log("Changed filters !");
  }


  return(
    <div id="filter" className="d-flex flex-column flex-lg-row col-12">
      <Navbar/>
      <div className="col-lg-10 p-0 p-3 d-flex flex-column">
        <div className="dash-tab top-infos p-1 mb-3">
          <form onSubmit={handleChangeFilters}>
            <div className="form-group d-flex flex-wrap">
              <select className="form-control col-sm-2 m-1">
                <option value="">Sélectionnez un animal</option>

              </select>
              <select className="form-control col-sm-2 m-1">
                <option value="">Sélectionnez un fruit</option>
              </select>
              <select className="form-control col-sm-2 m-1">
                <option value="">Selectionnez une couleur</option>
              </select>
              <select className="form-control col-sm-2 m-1">
                <option value="">Selectionnez un Pays</option>
              </select>
              <select className="form-control col-sm-2 m-1">
                <option value="">Selectionnez</option>
              </select>
              <select className="form-control col-sm-2 m-1">
                <option value="">Selectionnez</option>
              </select>
              <select className="form-control col-sm-2 m-1">
                <option value="">Selectionnez</option>
              </select>
            </div>
            <div className="d-flex flex-row justify-content-center">
              <button type="submit" className="btn btn-primary col-sm-2 m-0">Valider les filtres</button>
            </div>
          </form>
        </div>
        <div className="dash-tab main-tab main-tab-filter p-3 mb-3 table-responsive">
          <table id="all-table" className="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nom</th>
                <th scope="col">Prénom</th>
                <th scope="col">Sexe</th>
                <th scope="col">Email</th>
                <th scope="col">Adresse</th>
                <th scope="col">Ville</th>
                <th scope="col">Pays</th>
                <th scope="col">Téléphone</th>
                <th scope="col">Position</th>
                <th scope="col">Animal préféré</th>
                <th scope="col">Fruit préféré</th>
                <th scope="col">Couleur préférée</th>
                <th scope="col">Film préféré</th>
              </tr>
            </thead>
            <tbody>
              {
                peoples.map(function(item, index){
                  return(
                    <tr className={`data-row data-row-${item.id}`} onClick={() => handleRowClick(item.id)} key={index}>
                      <th scope="row">{item.id}</th>
                      <td className={`data-content-lastname-${item.id}`}>{item.lastname}</td>
                      <td className={`data-content-firstname-${item.id}`}>{item.firstname}</td>
                      <td className={`data-content-gender-${item.id}`}>{item.gender}</td>
                      <td className={`data-content-email-${item.id}`}>{item.contact.email}</td>
                      <td className={`data-content-address-${item.id}`}>{item.contact.address}</td>
                      <td className={`data-content-city-${item.id}`}>{item.contact.city}</td>
                      <td className={`data-content-country-${item.id}`}>{item.contact.country}</td>
                      <td className={`data-content-phone-${item.id}`}>{item.contact.phone}</td>
                      <th className={`data-content-location-${item.id}`}>{item.contact.location.lon} {item.contact.location.lat}</th>
                      <td className={`data-content-pet-${item.id}`}>{item.preferences.favorite_pet}</td>
                      <td className={`data-content-fruit-${item.id}`}>{item.preferences.favorite_fruit}</td>
                      <td className={`data-content-color-${item.id}`}>{item.preferences.favorite_color}</td>
                      <td className={`data-content-movie-${item.id}`}>{item.preferences.favorite_movie}</td>
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default Filter;

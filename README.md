# Data Visualizer - Projet Front-end
###### [Enzo AVAGLIANO](https://github.com/EloxFire) - B3 Info Web



### Rappel de l'exercice :

Créer un projet front-end en utilisant des données mises à disposition, votre projet répondra au cahier des charges détaillé.



### Données à utiliser :
[Lien vers l'API à utiliser](https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7)



### Fonctionnalités :

- [x] Tableau paginé des données
- [x] Recherche universelle dans les données
- [ ] Filtrage des données
- [ ] Visualisation des données sous forme de graphiques
  - [x] Camemberts
  - [x] Histogrammes
- [x] Consulter le détail de chaque ligne du tableau
- [ ] Modifier les données du tableau
- [x] Exporter les données du tableau sous format JSON

**BONUS :**

- [ ] Filtrer les données en cliquant sur les graphiques
- [x] Visualiser les données sur une carte géographique
- [x] Implémenter des tests unitaires



### Installation & Utilisation du projet :

1. Cloner le projet

   ```bash
   git clone https://gitlab.com/ynov-cours/data-visualizer-b3-front.git
   ```

2. Installer les dépendances

   ```bash
   cd data-visualizer-b3-front && npm i
   ```

3. Démarrer le projet

   ```bash
   npm start
   ```



#### Dépendances utilisées :

- React-chart-js-2
- React-minimal-pie-chart
- Bootstrap
- JQuery
- React-Icons



Projet créé avec ReactJS

- [create-react-app by facebook](https://github.com/facebook/create-react-app)

- [Documentation de ReactJS](https://reactjs.org/docs/getting-started.html)
